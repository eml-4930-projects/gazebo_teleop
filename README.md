# Teleop ROS2 Package
For this project you will create a ROS2 node that will:
* Subscribe to a controller topic (joy).
* Publish a twist message (geometry_msgs/Twist) to control the steering and velocity of a go-kart in Ignition Gazebo.

## Package Overview
```mermaid
graph LR
    A(joy_node) -->|sensor_msgs/joy| B(teleop_node)
    B -->|geometry_msgs/Twist| C(ignition gazebo)
```

You will be creating the __teleop_node__ in the diagram above. Follow the links below for more information on creating your ROS2 package/node.

* [Node](files/node.md)
* [CMake](files/cmake.md)
* [Launch](files/launch.md)

## Class Support Repository
Follow the instructions in the [README](https://gitlab.com/eml-4930-projects/class_resources) to setup the support repository.
__You need this for integration with Ignition Gazebo for the class__. 

## Running node
__Make sure ROS2 is sourced or commands will not work__. The below command should ideally be in your .bashrc file so that ROS2 is sourced evertime you create a new terminal.
```bash
source /opt/ros/foxy/setup.bash
```
Example command to launch project. Replace package and launch names with appropriate names.
```bash
ros2 launch my_package_name your_launch_name.launch.py
```
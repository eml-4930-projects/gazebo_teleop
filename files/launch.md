# Launch

Create a directory in your package directory called "launch". You can do this with the file explorer or terminal command:
```bash
mkdir launch
```
Then create a launch file, within the "launch" directory, with a name you choose but following the form:
```bash
touch your_launch_name.launch.py
```

There are two additional ROS2 nodes you will need to run for this project. The format of their launch entries are detailed more below.

## Gamepad
You can use joy_node to received inputs from Xbox, Playstation, or generic gamepads that can connect to your computer.
The launch configuration below shows the all possible node parameters at their default values. These dont need to be set unless you intend on changing a parameter's value. 

```python
joy_node = Node(
    package='joy',
    executable='joy_node',
    parameters = [
        {'device_id': 0},
        {'device_name': ''},
        {'autorepeat_rate': 20.0},
        {'deadzone': 0.05},
        {'sticky_buttons': False},
        {'coalesce_interval_ms': 1}
    ]
)
```
A detailed description of parameters can be found [here](https://github.com/ros-drivers/joystick_drivers/tree/ros2/joy).

## Ignition - ROS2 Bridge
```python
"""
    The meaning of the symbols after the ROS message type
    @ is a bidirectional bridge.
    [ is a bridge from Ignition to ROS.
    ] is a bridge from ROS to Ignition.
    
    Format:
        topic_name@ros message name([ or ] or @)ignition message name
"""
bridge = Node(
    package='ros_ign_bridge',
    executable='parameter_bridge',
    arguments=['/model/navigator_agk/cmd_vel@geometry_msgs/msg/Twist]ignition.msgs.Twist',
                '/navsat@sensor_msgs/msg/NavSatFix[ignition.msgs.NavSat',
                '/clock@rosgraph_msgs/msg/Clock[ignition.msgs.Clock'],
    output='screen'
)
```

## Complete Launch File
**You should replace entries that contain "your" with the specific name of your package and executable** The executable name will be the one set in the CMake.

```python
import os
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch_ros.actions import Node
from launch.launch_description_sources import PythonLaunchDescriptionSource
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    ld = LaunchDescription()

    joy_node = Node(
        package='joy',
        executable='joy_node',
        parameters = [
            {'autorepeat_rate': 10.0}]
    )

    your_node = Node(
        package='your_package_name',
        executable='your_node_name',
        output='screen',
    )

    # Comment the block below and "ld.add_action(bridge)" if you
    # want to test launch file without launching ros-gazebo bridge each time.
    bridge = Node(
        package='ros_ign_bridge',
        executable='parameter_bridge',
        arguments=['/model/navigator_agk/cmd_vel@geometry_msgs/msg/Twist]ignition.msgs.Twist',
                   '/clock@rosgraph_msgs/msg/Clock[ignition.msgs.Clock'],
        output='screen'
    )

    gokart_launch = get_package_share_directory('gokart_launch')

    # Comment the block below and "ld.add_action(ign_gazebo)" if you
    # want to test launch file without launching gazebo each time.
    ign_gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(gokart_launch, 'launch', 'ign_gazebo.launch.py')),
        launch_arguments={
            'ign_args': 'gokart_teleop.world'
        }.items(),
    )

    ld.add_action(joy_node)
    ld.add_action(your_node)
    ld.add_action(bridge)
    ld.add_action(ign_gazebo)

    return ld
```
# CMake

Here is a completed CMake file that you can copy and replace the default file.

__Replace any reference to "package_name" and "your_node_name" with names created by you.__

```cmake
cmake_minimum_required(VERSION 3.5)

project(package_name) # Change name to match your package

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(geometry_msgs REQUIRED)

add_executable(your_node_name src/your_node_name.cpp)
ament_target_dependencies(your_node_name rclcpp sensor_msgs geometry_msgs)

install(TARGETS
  your_node_name
  DESTINATION lib/${PROJECT_NAME}
)

install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}
)

ament_package()
```
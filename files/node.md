# ROS2 Node
## Description
Your package should contain one ROS2 node, written in C++, that is responsible for translating controller inputs to simulator inputs.

It is recommended to use [the subscriber example](https://github.com/ros2/examples/blob/foxy/rclcpp/topics/minimal_subscriber/member_function.cpp) as the template for your node. You should rename the class and callback function were ever appropriate.

## Publisher
----
The ignition gazebo plugin we are using expects a __geometry_msgs/Twist__ message to be published that contains:
*  The desired forward velocity along x-axis (m/s)
*  The desired angular velocity about z-axis (yaw rate) (rad/s)

You will need to setup a publisher that will publish __geometry_msgs/Twist__ messages. [Here is an example of a publisher written in C++](https://github.com/ros2/examples/blob/foxy/rclcpp/topics/minimal_publisher/member_function.cpp)

The topic name should be "/model/navigtor_agk/cmd_vel"

## Subscriber
----
Controller inputs from "joy_node" are published using the "joy" topic name. You will need to setup a subscriber for this topic and create a callback function that will perform any necessary conversions and publish a Twist message.

The selection of controller inputs to map to the Twist velocities is left up to you but you should use axes that vary between [1, -1]. To see which controller inputs map to what values in the **joy** message you can start the joy_node.
```bash
ros2 run joy joy_node
```
Then echo the **joy** topic in another terminal to see the values change as you press the different buttons, move the sticks, and press the triggers.
```bash
ros2 topic echo /joy
```

### Example Skeleton Callback
Documentation on the structure of the joy message can be found [here](https://docs.ros.org/en/api/sensor_msgs/html/msg/Joy.html). "axes" and "buttons" are [c++ vectors](https://www.programiz.com/cpp-programming/vectors).
```cpp
void joy_callback(const sensor_msgs::msg::Joy::SharedPtr msg) const
{
    // Data Structure that will be published
    geometry_msgs::msg::Twist twist_msg;

    double wheelbase = 1.0414; // (meters) Magic number, matches simulator
    
    /* Access the desired controller axis from "msg" and map them
     * to the desired values for twist. The "msg" variable is a
     * pointer so you must use the arrow operator to access the 
     * elements of "msg". Ex: msg->axes
     *
     * Forward velocity should range from [0, 5] m/s
     * Steering angle should range from [-30, 30] degrees (convert this 
     * radians when doing any math)
     * 
     * Forward velocity: (m/s)
     *      twist_msg.linear.x = some_value;
     *
     * Angular Velocity: (rad/s) (this one will require some math)
     *      twist_msg.angular.z = some_value;
     */

     twist_pub_->publish(twist_msg); // Publisher is called twist_pub_
}
```

## Solving for yaw rate (angular.z)
![bicyle_geometry](../images/bicyle_diagram.png)
